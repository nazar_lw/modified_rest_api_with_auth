package ua.com.epam.securityconfigs;

import java.util.Objects;

class CustomUserDetails {

    private String username;
    private String password;

    public CustomUserDetails() {
    }

    public CustomUserDetails(String username, String password) {
        this.username = username;
        this.password = password;
    }

    String getUsername() {
        return username;
    }
    String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomUserDetails that = (CustomUserDetails) o;
        return Objects.equals(username, that.username) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password);
    }

    @Override
    public String toString() {
        return "CustomUserDetails{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
